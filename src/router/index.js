import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Register from '@/components/Register'
import Courier from '@/components/Courier'
import Checkout from '@/components/Checkout'
import RegisterClient from '@/components/RegisterClient'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/courier',
      redirect: '/courier/register'
    },
    {
      path: '/courier/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/courier/dashboard',
      name: 'Courier',
      component: Courier
    },
    {
      path: '/client/register',
      name: 'RegisterClient',
      component: RegisterClient
    },
    {
      path: '/client',
      redirect: 'client/register'
    },
    {
      path: '/client/checkout',
      name: 'Checkout',
      component: Checkout
    }
  ]
})
