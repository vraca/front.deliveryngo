// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueSocketio from 'vue-socket.io'
import * as VueGoogleMaps from 'vue2-google-maps'
import config from '../config/index'
import ToggleButton from 'vue-js-toggle-button'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueSocketio, config.wsEndpoint)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAFTcxsk4JfgqcChWzEgd7Vs4a5BFWVmIU',
    libraries: 'places',
    autobindAllEvents: false
  }
})
Vue.use(ToggleButton)

export const bus = new Vue()
export let SOCKET_LISTEN_STATE = 1

bus.$on('toggle_socket_state', value => {
  console.log('change socket state to ' + value)
  SOCKET_LISTEN_STATE = value
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  sockets: {
    connect: function () {
      console.log('socket connected')
    },
    'get_location': function (data) {
      if (SOCKET_LISTEN_STATE) {
        bus.$emit('get_location', data)
      }
    },
    'courier_eligible': function (data) {

    },
    'order_confirmed': function (data) {
    },
    'add_courier': function (data) {
      bus.$emit('add_courier', data)
    },
    'new_order_request': function (data) {
      if (SOCKET_LISTEN_STATE) {
        bus.$emit('new_order_request', data)
      }
    }
  }
})
